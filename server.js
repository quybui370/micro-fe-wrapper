const http = require('http');
const path = require('path');
const Tailor = require('node-tailor');
const tailor = new Tailor({
  templatesPath: path.join(__dirname, 'templates')
});

// Root Server
http.createServer((req, res) => {
    tailor.requestHandler(req, res);
  })
  .listen(8080, function () {
    console.log('Tailor server listening on port 8080');
  });